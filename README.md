# README #

Many to many extension for Yii2<br/>
For many to many relations, for example:

```sql
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `category_product` (
  `category_id` int(4) NOT NULL,
  `product_id` int(4) NOT NULL,
  PRIMARY KEY (`category_id`,`product_id`),
  KEY `category_id` (`category_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

```php
<?php
namespace app\models;
use alexs\yii2manytomany\ManyToMany;
use yii\db\ActiveRecord;

class Category extends ActiveRecord
{
    // array of id's
    public $product_id = [];
    
    // ...

    public function rules() {
        return [
            ['product_id', 'each', 'rule'=>['integer']],
            // ...
        ];
    }

    public function behaviors() {
        return [
            [
                'class'=>ManyToMany::className(),
                'relations'=>[
                    'category_product'=>[
                        'category_id',
                        'product_id',
                    ],
                ],
            ],
            // ...
        ];
    }
}
```

With custom attributes and/or filters

```php
<?php
namespace app\models;
use alexs\yii2manytomany\ManyToMany;
use yii\db\ActiveRecord;

class Category extends ActiveRecord
{
    // array of id's
    public $product_id = [];
    
    // ...

    public function rules() {
        return [
            ['product_id', 'each', 'rule'=>['integer']],
            // ...
        ];
    }

    public function behaviors() {
        return [
            [
                'class'=>ManyToMany::className(),
                'relations'=>[
                    'category_product'=>[
                        'category_id',
                        'product_id',
                        [
                            'pos', // existing attribute
                            'custom_attribute2'=>function($val) {
                                return $val + 9999;
                            },
                            'custom_attribute3'=>123,
                        ],
                        // filter empty values
                        // @see http://php.net/manual/en/function.array-filter.php
                        'filter'=>function(array $row) {
                            return trim($row[2]) !== '';
                        },
                    ],
                ],
            ],
            // ...
        ];
    }
}
```

The solution for relations in the same table, for example:

```sql
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `category_related` (
  `category_id` int(4) NOT NULL,
  `related_category_id` int(4) NOT NULL,
  PRIMARY KEY (`category_id`,`related_category_id`),
  KEY `category_id` (`category_id`),
  KEY `related_category_id` (`related_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

```php
<?php
namespace app\models;
use alexs\yii2manytomany\OwnManyToMany;
use yii\db\ActiveRecord;

class Category extends ActiveRecord
{
    // array of id's
    public $related_category_id = [];
    // ...

    public function rules() {
        return [
            ['related_category_id', 'each', 'rule'=>['integer']],
            // ...
        ];
    }

    public function behaviors() {
        return [
            [
                'class'=>OwnManyToMany::className(),
                'relations'=>[
                    'category_related'=>[
                        'category_id',
                        'related_category_id',
                    ],
                ],
            ],
            // ...
        ];
    }
}
```
