<?php
/**
 * @class   ManyToMany
 * @author  Alex Sergey (createtruesite@gmail.com)
 *
 * Many to Many Relation Behavior
 */

namespace alexs\yii2manytomany;
use yii\db\ActiveRecord;
use yii\base\Behavior;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;

class ManyToMany extends Behavior
{
    public $relations = [];
    // make sense without the foreign keys
    public $delete_relations = false;
    // a filter of data before insert
    public $filter = NULL;
    // insert only unique relations
    public $unique_relations = true;
    
    public function events() {
        return [
            ActiveRecord::EVENT_AFTER_FIND  =>'afterFind',
            ActiveRecord::EVENT_AFTER_INSERT=>'afterInsert',
            ActiveRecord::EVENT_AFTER_UPDATE=>'afterUpdate',
            ActiveRecord::EVENT_AFTER_DELETE=>'afterDelete',
        ];
    }
    
    public function afterFind() {
        $this->fillRelationsValues();
    }
    
    public function afterInsert() {
        $this->insertRelations(false);
    }
    
    public function afterUpdate() {
        $this->insertRelations(true);
    }
    
    public function afterDelete() {
        if ($this->delete_relations) {
            $this->deleteRelations();    
        }
    }
    
    protected function fillRelationsValues() {
        /** @var ActiveRecord $Model */
        $Model = $this->owner;
        $connection = $Model::getDb();
        foreach ($this->relations as $table_name=>$relation) {
            list($rel_attr, $attr) = $relation;
            $custom_attrs = ArrayHelper::getValue($relation, 2);
            $sql = 'SELECT * FROM ' . $table_name . ' WHERE ' . $rel_attr . '=:' . $rel_attr;
            $sql_params = [':' . $rel_attr=>$Model->primaryKey];
            $all = $connection->createCommand($sql, $sql_params)->queryAll();
            $Model->{$attr} = ArrayHelper::getColumn($all, $attr);
            if (!empty($custom_attrs)) {
                foreach ($custom_attrs as $_custom_attr=>$custom_attr) {
                    if (!is_string($_custom_attr)) {
                        $Model->{$custom_attr} = ArrayHelper::getColumn($all, $custom_attr);
                    } else {
                        $Model->{$_custom_attr} = ArrayHelper::getColumn($all, $_custom_attr);
                    }
                }
            }
        }
    }
    
    protected function insertRelations($delete_relations) {
        /** @var ActiveRecord $Model */
        $Model = $this->owner;
        $connection = $Model::getDb();
        $transaction = $connection->beginTransaction();
        try {
            if ($delete_relations) {
                // delete old relations
                $this->deleteRelations();      
            }
            // insert new relations
            foreach ($this->relations as $table_name=>$relation) {
                list($rel_attr, $attr) = $relation;
                $custom_attrs = ArrayHelper::getValue($relation, 2);
                $insert_cols = [$rel_attr, $attr];
                $insert_rows = [];
                if (empty($custom_attrs)) {
                    if (!empty($Model->{$attr}) && is_array($Model->{$attr})) {
                        foreach ($Model->{$attr} as $attr_val) {
                            $insert_rows[] = [$Model->primaryKey, $attr_val];
                        }
                    }
                } else {
                    foreach ($custom_attrs as $_custom_attr=>$custom_attr) {
                        if (!is_string($_custom_attr)) {
                            $insert_cols[] = $custom_attr;
                        } else {
                            $insert_cols[] = $_custom_attr;
                        }
                    }
                    if (!empty($Model->{$attr}) && is_array($Model->{$attr})) {
                        foreach ($Model->{$attr} as $attr_key => $attr_val) {
                            $insert_row = [$Model->primaryKey, $attr_val];
                            foreach ($custom_attrs as $_custom_attr => $custom_attr) {
                                if (!is_string($_custom_attr)) {
                                    // just get a custom attribute value
                                    $custom_attr_val = ArrayHelper::getValue((array)$Model->{$custom_attr}, $attr_key);
                                } else {
                                    // get and process a custom attribute value
                                    $custom_attr_val = $this->processCustomAttributeValue(
                                        $_custom_attr,
                                        ArrayHelper::getValue((array)$Model->{$_custom_attr}, $attr_key),
                                        $custom_attr
                                    );
                                }
                                $insert_row[] = $custom_attr_val;
                            }
                            $insert_rows[] = $insert_row;
                        }
                    }
                }
                if ($this->unique_relations) {
                    $insert_rows = array_unique($insert_rows, SORT_REGULAR);
                }
                // filter
                if ($this->filter !== NULL) {
                    $insert_rows = $this->filterRows($insert_rows);
                }
                // insert to the database
                if (!empty($insert_rows)) {
                    $connection->createCommand()
                               ->batchInsert($table_name, $insert_cols, $insert_rows)
                               ->execute();
                }
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollback();
            throw $e;
        }
    }

    protected function processCustomAttributeValue($custom_attr, $custom_attr_val, $custom_attr_method) {
        if (($custom_attr_method instanceof \Closure) || is_array($custom_attr_method)) {
            if (!is_callable($custom_attr_method)) {
                throw new InvalidParamException('Invalid value of an attribute ' . $custom_attr);
            }
            return call_user_func($custom_attr_method, $custom_attr_val);
        }
        if (is_callable($custom_attr_method)) {
            return call_user_func($custom_attr_method, $custom_attr_val);
        }
        // is simple 'custom_attr'=>123
        return $custom_attr_method;
    }
    
    protected function deleteRelations() {
        /** @var ActiveRecord $Model */
        $Model = $this->owner;
        $connection = $Model::getDb();
        foreach ($this->relations as $table_name=>$relation) {
            list($rel_attr) = $relation;
            $connection->createCommand()
                       ->delete($table_name, [$rel_attr=>$Model->primaryKey])
                       ->execute();
        }
    }
    
    protected function filterRows(array $insert_rows) {
        if (!is_callable($this->filter)) {
            throw new InvalidParamException('Filter method cannot be called');
        }
        return array_filter($insert_rows, $this->filter);
    }
}
