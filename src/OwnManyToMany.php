<?php
/**
 * @class   ManyToMany
 * @author  Alex Sergey (createtruesite@gmail.com)
 *
 * Many to Many Relation Behavior
 */

namespace alexs\yii2manytomany;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class OwnManyToMany extends ManyToMany
{
    protected function fillRelationsValues() {
        /** @var ActiveRecord $Model */
        $Model = $this->owner;
        $connection = $Model::getDb();
        foreach ($this->relations as $table_name=>$relation) {
            list($rel_attr, $attr) = $relation;
            $some_attrs = ArrayHelper::getValue($relation, 2);
            // A=>B
            $sql = 'SELECT * FROM ' . $table_name . ' WHERE ' . $rel_attr . '=:' . $rel_attr;
            $sql_params = [':' . $rel_attr=>$Model->primaryKey];
            $all = $connection->createCommand($sql, $sql_params)->queryAll();
            $Model->{$attr} = ArrayHelper::getColumn($all, $attr);
            // B=>A
            $sql = 'SELECT * FROM ' . $table_name . ' WHERE ' . $attr . '=:' . $rel_attr;
            $all = $connection->createCommand($sql, $sql_params)->queryAll();
            $Model->{$attr} = array_merge($Model->{$attr}, ArrayHelper::getColumn($all, $rel_attr));
            if (!empty($some_attrs)) {
                foreach ($some_attrs as $_some_attr=>$some_attr) {
                    if (!is_string($_some_attr)) {
                        $Model->{$some_attr} = ArrayHelper::getColumn($all, $some_attr);
                    } else {
                        $Model->{$_some_attr} = ArrayHelper::getColumn($all, $_some_attr);
                    }
                }
            }
        }
    }

    protected function deleteRelations() {
        /** @var ActiveRecord $Model */
        $Model = $this->owner;
        $connection = $Model::getDb();
        foreach ($this->relations as $table_name=>$relation) {
            list($rel_attr, $attr) = $relation;
            $sql_params = [':' . $rel_attr=>$Model->primaryKey];
            $connection->createCommand()
                       ->delete($table_name, $rel_attr . '=:' . $rel_attr . ' OR ' . $attr . '=:' . $rel_attr, $sql_params)
                       ->execute();
        }
    }
}
