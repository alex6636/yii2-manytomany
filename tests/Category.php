<?php
namespace alexs\yii2manytomany\tests;
use yii\db\ActiveRecord;

class Category extends ActiveRecord
{
    public $product_id = [];
    public $custom_attr1 = [];
    public $custom_attr2 = [];
    public $related_category_id = [];

    public function rules() {
        return [
            [['product_id', 'related_category_id'], 'each', 'rule'=>['integer']],
            [['custom_attr1', 'custom_attr2'], 'filter', 'filter'=>function($value) {
                return array_map('trim', $value);
            }],
            ['title', 'string'],
        ];
    }
}
