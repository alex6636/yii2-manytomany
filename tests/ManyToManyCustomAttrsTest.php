<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   11-Sep-17
 */

namespace alexs\yii2sluggable\tests;
use alexs\yii2phpunittestcase\DatabaseTestCase;
use alexs\yii2manytomany\tests\Category;
use alexs\yii2manytomany\ManyToMany;
use Yii;

class ManyToManyCustomAttrsTest extends DatabaseTestCase
{
    public function testCustomAttr() {
        $Category = new Category;
        $Category->attachBehavior('ManyToMany', [
            'class'=>ManyToMany::className(),
            'relations'=>[
                'category_product'=>[
                    'category_id',
                    'product_id',
                    [
                        'custom_attr1',
                        'custom_attr2'=>function($val) {
                            return $val . '_updated';
                        }
                    ],
                ],
            ],
        ]);
        $Category->setAttributes([
            'id'=>1,
            'title'=>'First category',
            'product_id'=>[1, 2, 3],
            'custom_attr1'=>['custom1', 'custom2', 'custom3'],
            'custom_attr2'=>['custom1', 'custom2', 'custom3'],
        ]);
        $Category->save();
        $this->assertEquals([
            ['category_id'=>1, 'product_id'=>1, 'custom_attr1'=>'custom1', 'custom_attr2'=>'custom1_updated'],
            ['category_id'=>1, 'product_id'=>2, 'custom_attr1'=>'custom2', 'custom_attr2'=>'custom2_updated'],
            ['category_id'=>1, 'product_id'=>3, 'custom_attr1'=>'custom3', 'custom_attr2'=>'custom3_updated'],
        ], $this->getRelations());
    }

    protected function getRelations() {
        return Yii::$app->db->createCommand('SELECT * FROM category_product')->queryAll();
    }

    protected function setUp() {
        parent::setUp();
        Yii::$app->db->createCommand()->createTable('category', [
            'id'   =>'pk',
            'title'=>'string NOT NULL',
        ])->execute();
        Yii::$app->db->createCommand()->createTable('category_product', [
            'category_id'=>'int NOT NULL',
            'product_id' =>'int NOT NULL',
            'custom_attr1'=>'string DEFAULT NULL',
            'custom_attr2'=>'string DEFAULT NULL',
        ])->execute();
    }

    protected function tearDown() {
        Yii::$app->db->createCommand()->dropTable('category')->execute();
        Yii::$app->db->createCommand()->dropTable('category_product')->execute();
        parent::tearDown();
    }
}
