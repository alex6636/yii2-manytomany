<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   11-Sep-17
 */

namespace alexs\yii2sluggable\tests;
use alexs\yii2phpunittestcase\DatabaseTestCase;
use alexs\yii2manytomany\tests\Category;
use alexs\yii2manytomany\ManyToMany;
use Yii;

class ManyToManyTest extends DatabaseTestCase
{
    public function testInsertUpdate() {
        $Category = new Category;
        $Category->attachBehavior('ManyToMany', [
            'class'=>ManyToMany::className(),
            'relations'=>[
                'category_product'=>[
                    'category_id',
                    'product_id',
                ],
            ],
        ]);
        $Category->setAttributes([
            'id'=>1,
            'title'=>'First category',
            'product_id'=>[1, 2, 3],
        ]);
        $Category->save();
        $this->assertSame([1, 2, 3], $Category->product_id);
        $this->assertEquals([
            ['category_id'=>1, 'product_id'=>1],
            ['category_id'=>1, 'product_id'=>2],
            ['category_id'=>1, 'product_id'=>3],
        ], $this->getRelations());

        $Category->product_id = [1, 3];
        $Category->save();
        $this->assertEquals([
            ['category_id'=>1, 'product_id'=>1],
            ['category_id'=>1, 'product_id'=>3],
        ], $this->getRelations());

        $Category->product_id = [];
        $Category->save();
        $this->assertEquals([
        ], $this->getRelations());
    }

    public function testDeleteAndSaveRelations() {
        $Category = new Category;
        $Category->attachBehavior('ManyToMany', [
            'class'=>ManyToMany::className(),
            'relations'=>[
                'category_product'=>[
                    'category_id',
                    'product_id',
                ],
            ],
        ]);
        $Category->setAttributes([
            'id'=>1,
            'title'=>'First category',
            'product_id'=>[1, 2, 3],
        ]);
        $Category->save();
        $Category->delete();
        $this->assertEquals([
            ['category_id'=>1, 'product_id'=>1],
            ['category_id'=>1, 'product_id'=>2],
            ['category_id'=>1, 'product_id'=>3],
        ], $this->getRelations());
    }

    public function testDeleteAndDeleteRelations() {
        $Category = new Category;
        $Category->attachBehavior('ManyToMany', [
            'class'=>ManyToMany::className(),
            'relations'=>[
                'category_product'=>[
                    'category_id',
                    'product_id',
                ],
            ],
            'delete_relations'=>true,
        ]);
        $Category->setAttributes([
            'id'=>1,
            'title'=>'First category',
            'product_id'=>[1, 2, 3],
        ]);
        $Category->save();
        $Category->delete();
        $this->assertEquals([

        ], $this->getRelations());
    }

    public function testUniqueRelations() {
        $Category = new Category;
        $Category->attachBehavior('ManyToMany', [
            'class'=>ManyToMany::className(),
            'relations'=>[
                'category_product'=>[
                    'category_id',
                    'product_id',
                ],
            ],
        ]);
        $Category->setAttributes([
            'id'=>1,
            'title'=>'First category',
            'product_id'=>[1, 1, 2, 2],
        ]);
        $Category->save();
        $this->assertEquals([
            ['category_id'=>1, 'product_id'=>1],
            ['category_id'=>1, 'product_id'=>2],
        ], $this->getRelations());
    }

    public function testFilter() {
        $Category = new Category;
        $Category->attachBehavior('ManyToMany', [
            'class' => ManyToMany::className(),
            'relations' => [
                'category_product' => [
                    'category_id',
                    'product_id',
                ],
            ],
            'filter'=>function(array $val) {
                return $val[1] % 2 === 0;
            },
        ]);
        $Category->setAttributes([
            'id' => 1,
            'title' => 'First category',
            'product_id' => [1, 2, 3],
        ]);
        $Category->save();
        $this->assertEquals([
            ['category_id'=>1, 'product_id'=>2],
        ], $this->getRelations());
    }

    protected function getRelations() {
        return Yii::$app->db->createCommand('SELECT * FROM category_product')->queryAll();
    }

    protected function setUp() {
        parent::setUp();
        Yii::$app->db->createCommand()->createTable('category', [
            'id'   =>'pk',
            'title'=>'string NOT NULL',
        ])->execute();
        Yii::$app->db->createCommand()->createTable('category_product', [
            'category_id'=>'int NOT NULL',
            'product_id' =>'int NOT NULL',
        ])->execute();
    }

    protected function tearDown() {
        Yii::$app->db->createCommand()->dropTable('category')->execute();
        Yii::$app->db->createCommand()->dropTable('category_product')->execute();
        parent::tearDown();
    }
}
