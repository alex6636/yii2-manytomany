<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   11-Sep-17
 */

namespace alexs\yii2sluggable\tests;
use alexs\yii2phpunittestcase\DatabaseTestCase;
use alexs\yii2manytomany\tests\Category;
use alexs\yii2manytomany\OwnManyToMany;
use Yii;

class OwnManyToManyTest extends DatabaseTestCase
{
    public function testInsertUpdate() {
        $Category = new Category;
        $Category->attachBehavior('ManyToMany', [
            'class'=>OwnManyToMany::className(),
            'relations'=>[
                'category_related'=>[
                    'category_id',
                    'related_category_id',
                ],
            ],
        ]);
        $Category->setAttributes([
            'id'=>1,
            'title'=>'First category',
            'related_category_id'=>[1, 2, 3],
        ]);
        $Category->save();
        $this->assertEquals([
            ['category_id'=>1, 'related_category_id'=>1],
            ['category_id'=>1, 'related_category_id'=>2],
            ['category_id'=>1, 'related_category_id'=>3],
        ], $this->getRelations());
    }

    public function testDeleteAndDeleteRelations() {
        $Category = new Category;
        $Category->attachBehavior('ManyToMany', [
            'class'=>OwnManyToMany::className(),
            'relations'=>[
                'category_related'=>[
                    'category_id',
                    'related_category_id',
                ],
            ],
            'delete_relations'=>true,
        ]);
        $Category->setAttributes([
            'id'=>1,
            'title'=>'First category',
            'related_category_id'=>[1, 2, 3],
        ]);
        $Category->save();
        $Category->delete();
        $this->assertEquals([

        ], $this->getRelations());
    }

    protected function getRelations() {
        return Yii::$app->db->createCommand('SELECT * FROM category_related')->queryAll();
    }

    protected function setUp() {
        parent::setUp();
        Yii::$app->db->createCommand()->createTable('category', [
            'id'   =>'pk',
            'title'=>'string NOT NULL',
        ])->execute();
        Yii::$app->db->createCommand()->createTable('category_related', [
            'category_id'        =>'int NOT NULL',
            'related_category_id'=>'int NOT NULL',
        ])->execute();
    }

    protected function tearDown() {
        Yii::$app->db->createCommand()->dropTable('category')->execute();
        Yii::$app->db->createCommand()->dropTable('category_related')->execute();
        parent::tearDown();
    }
}
